## Changelog

**v0.0.2**
- Terrain fixed. (from 20 fps to 40 fps //collisions//)
- License fixed.
- Funcs added ( **API** ).
- Examples added.

**v0.0.1**
- AddObject function
- Terrain added.
- Basic API